package edu.xda.cosmeticonlineshop.model;

/**
 * Created by Administrator on 10/05/2017.
 */

public class LoaiSanPha {
    private int id;
    private String  tenloaisp;
    private String hinhanhLoaisp;

    public LoaiSanPha(int id, String tenloaisp, String hinhanhLoaisp) {
        this.id = id;
        this.tenloaisp = tenloaisp;
        this.hinhanhLoaisp = hinhanhLoaisp;
    }

    public LoaiSanPha() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTenloaisp() {
        return tenloaisp;
    }

    public void setTenloaisp(String tenloaisp) {
        this.tenloaisp = tenloaisp;
    }

    public String getHinhanhLoaisp() {
        return hinhanhLoaisp;
    }

    public void setHinhanhLoaisp(String hinhanhLoaisp) {
        this.hinhanhLoaisp = hinhanhLoaisp;
    }
}
