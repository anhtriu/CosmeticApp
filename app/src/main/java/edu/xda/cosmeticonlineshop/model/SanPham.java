package edu.xda.cosmeticonlineshop.model;

import java.io.Serializable;

/**
 * Created by Administrator on 10/05/2017.
 */

public class SanPham implements Serializable {
    private int id;
    private String tensp;
    private Integer giasp;
    private String hinhanhsp;
    private String motosp;
    private int maloaisp;

    public SanPham() {
    }

    public SanPham(int id, String tensp, Integer giasp, String hinhanhsp, String motosp, int maloaisp) {
        this.id = id;
        this.tensp = tensp;
        this.giasp = giasp;
        this.hinhanhsp = hinhanhsp;
        this.motosp = motosp;
        this.maloaisp = maloaisp;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTensp() {
        return tensp;
    }

    public void setTensp(String tensp) {
        this.tensp = tensp;
    }

    public Integer getGiasp() {
        return giasp;
    }

    public void setGiasp(Integer giasp) {
        this.giasp = giasp;
    }

    public String getHinhanhsp() {
        return hinhanhsp;
    }

    public void setHinhanhsp(String hinhanhsp) {
        this.hinhanhsp = hinhanhsp;
    }

    public String getMotosp() {
        return motosp;
    }

    public void setMotosp(String motosp) {
        this.motosp = motosp;
    }

    public int getMaloaisp() {
        return maloaisp;
    }

    public void setMaloaisp(int maloaisp) {
        this.maloaisp = maloaisp;
    }
}
