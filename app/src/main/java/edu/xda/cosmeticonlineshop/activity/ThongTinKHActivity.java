package edu.xda.cosmeticonlineshop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import edu.xda.cosmeticonlineshop.R;
import edu.xda.cosmeticonlineshop.common.CheckConnection;
import edu.xda.cosmeticonlineshop.common.Server;

public class ThongTinKHActivity extends AppCompatActivity {
    EditText edttenkh,edtsdt,edtemail;
    Button btnxac_nhan,btn_huy;
    CheckConnection con = new CheckConnection();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thong_tin_kh);
      init();
        if(con.haveNetworkConnection(getApplicationContext()) ){
                xacnhan();
        }else {
            con.showErr(getApplicationContext(),"Vui long kiem tra ket noi internet");
        }
    }

    private void xacnhan() {
        btnxac_nhan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String ten = edttenkh.getText().toString().trim();
                final String sdt = edtsdt.getText().toString().trim();
                final String email = edtemail.getText().toString().trim();
                if(TextUtils.isEmpty(ten) && TextUtils.isEmpty(sdt)
                        && TextUtils.isEmpty(email))
                {
                    con.showErr(getApplicationContext(),"Moi ban vui long dien thong tin");
                }
                else {
                    RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, Server.pathTTKH, new Response.Listener<String>() {
                        @Override
                        public void onResponse(final String madonhang) {
                          if(Integer.parseInt(madonhang)>0){
                              RequestQueue Queue = Volley.newRequestQueue(getApplicationContext());
                              StringRequest request = new StringRequest(Request.Method.POST, Server.pathCHITIETDH, new Response.Listener<String>() {
                                  @Override
                                  public void onResponse(String response) {
                                    if(response.equals("1")){
                                        MainActivity.manggiohang.clear();
                                        con.showErr(getApplicationContext(),"Ban da them du lieu gio hang thanh cong");
                                        startActivity(new Intent(getApplicationContext(),MainActivity.class));
                                        con.showErr(getApplicationContext(),"Moi ban tiep tuc mua hang");
                                    }
                                      else {
                                        con.showErr(getApplicationContext(),"Some error");
                                    }
                                  }
                              }, new Response.ErrorListener() {
                                  @Override
                                  public void onErrorResponse(VolleyError error) {

                                  }
                              }){
                                  @Override
                                  protected Map<String, String> getParams() throws AuthFailureError {
                                      JSONArray jsonArray = new JSONArray();
                                      for(int i = 0;i<MainActivity.manggiohang.size();i++){
                                          JSONObject obj = new JSONObject();
                                          try {
                                              obj.put("madonhang",madonhang);
                                              obj.put("masanpham",MainActivity.manggiohang.get(i).getIdsp());
                                              obj.put("tensanpham",MainActivity.manggiohang.get(i).getTensp());
                                              obj.put("giasanpham",MainActivity.manggiohang.get(i).getGiasp());
                                              obj.put("soluongsp",MainActivity.manggiohang.get(i).getSoluongsp());

                                          } catch (JSONException e) {
                                              e.printStackTrace();
                                          }
                                          jsonArray.put(obj);
                                      }
                                      HashMap<String,String> hashMap = new HashMap<String, String>();
                                      hashMap.put("json",jsonArray.toString());
                                      return hashMap;
                                  }
                              };
                              Queue.add(request);
                          }

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }){
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            HashMap<String,String> params = new HashMap<String, String>();
                            params.put("tenkhachhang",ten);
                            params.put("sodienthoai",sdt);
                            params.put("email",email);
                            return params;
                        }
                    };
                    requestQueue.add(stringRequest);
                }
            }
        });
    }

    private void init() {
        edttenkh = (EditText) findViewById(R.id.tenkh);
        edtsdt = (EditText) findViewById(R.id.sdtkh);
        edtemail = (EditText) findViewById(R.id.emailkh);
        btnxac_nhan = (Button) findViewById(R.id.btnxacnhan);
        btn_huy = (Button) findViewById(R.id.btngoback);
    }
}
