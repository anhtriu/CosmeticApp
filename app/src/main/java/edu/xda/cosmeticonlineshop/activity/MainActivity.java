package edu.xda.cosmeticonlineshop.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ViewFlipper;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import edu.xda.cosmeticonlineshop.R;
import edu.xda.cosmeticonlineshop.adapter.LoaiSPAdapter;
import edu.xda.cosmeticonlineshop.adapter.SanPhamAdapter;
import edu.xda.cosmeticonlineshop.common.CheckConnection;
import edu.xda.cosmeticonlineshop.common.Server;
import edu.xda.cosmeticonlineshop.model.GioHang;
import edu.xda.cosmeticonlineshop.model.LoaiSanPha;
import edu.xda.cosmeticonlineshop.model.SanPham;

public class MainActivity extends AppCompatActivity {
    Toolbar toolbar;
    DrawerLayout drawerLayout;
    ViewFlipper viewFlipper;
    RecyclerView recyclerView;
    NavigationView navigationView;
    ListView listView;
    ArrayList<LoaiSanPha> arrLoaisp;
    ArrayList<SanPham> arrSp;
    SanPhamAdapter spAdapter;
    LoaiSPAdapter adapter;
    int id = 0;
    String tenloaisp = "";
    public static  ArrayList<GioHang>  manggiohang;
    String hinhanhloaisp="";
    CheckConnection con = new CheckConnection();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        checkAppIntro();
        init();
        setUpActionbar();

        if(con.haveNetworkConnection(getApplicationContext()) ){
            loadDuLieuLoaiSanPham();
            loadDulieuSanPhamMoiNhat();
        }else {
            con.showErr(getApplicationContext(),"Vui long kiem tra ket noi internet");
        }
        chuyenmanhinh();
    }

    private void checkAppIntro() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
                boolean isFisrtStart = preferences.getBoolean("isFisrt",true);
                if(isFisrtStart){
                    startActivity(new Intent(MainActivity.this,IntroActivity.class));
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putBoolean("isFisrt",false);
                    editor.apply();
//                    editor.commit();
                }
            }
        }).start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menugiohang:
                startActivity(new Intent(getApplicationContext(),GioHangActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }

    private void chuyenmanhinh() {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position){
                    case 0:
                        Intent intent = new Intent(MainActivity.this,MainActivity.class);
                        startActivity(intent);
                        drawerLayout.closeDrawer(GravityCompat.START);
                        break;
                    case 1:
                        Intent i = new Intent(MainActivity.this,SonHanQuocActivity.class);
                        startActivity(i);
                        i.putExtra("loaiSanpham",arrLoaisp.get(position).getId());
                        drawerLayout.closeDrawer(GravityCompat.START);
                        break;

                }
            }
        });
    }


    private void loadDulieuSanPhamMoiNhat() {
        final RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JsonArrayRequest arrayRequest = new JsonArrayRequest(Server.pathSPMOINHAT, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                int idsp= 0;
                String tensp="";
                Integer giasp=0;
                String hinhanhsp ="";
                String MOTAsp ="";
                int loaisp=0;
                if(response != null){
                    for(int i =0;i<response.length();i++){
                        Log.e("CO DU LIEU K",String.valueOf(response));
                        try {
                            JSONObject jsonObject = response.getJSONObject(i);
                            idsp = jsonObject.getInt("id");
                            giasp = jsonObject.getInt("giasp");
                            tensp = jsonObject.getString("tensp");
                            hinhanhsp = jsonObject.getString("hinhanhsp");
                            MOTAsp = jsonObject.getString("motosp");
                            loaisp = jsonObject.getInt("idloaisp");
                            arrSp.add(new SanPham(idsp,tensp,giasp,hinhanhsp,MOTAsp,loaisp));
                            adapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(arrayRequest);
    }

    private void loadDuLieuLoaiSanPham() {
        final RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JsonArrayRequest arrayRequest = new JsonArrayRequest(Server.pathLoaiSP, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                if(response != null){
                    for(int i =0;i<response.length();i++){
                        Log.e("CO DU LIEU K",String.valueOf(response));
                        try {
                            JSONObject jsonObject = response.getJSONObject(i);
                            id = jsonObject.getInt("id");
                            tenloaisp = jsonObject.getString("tenloaisp");
                            hinhanhloaisp = jsonObject.getString("hinhanhloaisp");
                            arrLoaisp.add(new LoaiSanPha(id,tenloaisp,hinhanhloaisp));
                            adapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
                arrLoaisp.add(new LoaiSanPha(3,"Liên hệ","http://www.myphamhanquocso1.com//data//news//2419//smile-25.jpg"));
                arrLoaisp.add(new LoaiSanPha(4,"Giới thiệu","http://angiang.shopdep24h.com/3000/2447/1414127560_son-aron-immortal-shopdep24h.jpg"));
                adapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(arrayRequest);
    }


    private void setUpActionbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(android.R.drawable.ic_menu_sort_by_size);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(GravityCompat.START);
            }
        });
    }

    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbarMain);
        viewFlipper = (ViewFlipper) findViewById(R.id.viewFlip);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewMain);
        navigationView = (NavigationView) findViewById(R.id.navigation);
        listView = (ListView) findViewById(R.id.listMain);
        drawerLayout = (DrawerLayout) findViewById(R.id.draw);
        arrLoaisp = new ArrayList<>();
        adapter = new LoaiSPAdapter(getApplicationContext(),arrLoaisp);
        arrLoaisp.add(new LoaiSanPha(0,"Home","https://www.thegioididong.com/samsung/Content/images/desktop/th.png"));
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        arrSp = new ArrayList<>();
        spAdapter = new SanPhamAdapter(getApplicationContext(),arrSp);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getApplicationContext(),2));
        recyclerView.setAdapter(spAdapter);
        if(manggiohang!=null){
        }
        else {
            manggiohang = new ArrayList<>();
        }
    }
}
