package edu.xda.cosmeticonlineshop.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.text.DecimalFormat;

import edu.xda.cosmeticonlineshop.R;
import edu.xda.cosmeticonlineshop.adapter.GioHangAdapter;
import edu.xda.cosmeticonlineshop.common.CheckConnection;

public class GioHangActivity extends AppCompatActivity {
    ListView listHangMua;
    GioHangAdapter ghAdapter;
    Button btnThanhtoan;
    Button btnMuatiep;
    Toolbar toolbar;
    TextView txtthongbao;
    static TextView txttongtien;
    CheckConnection con = new CheckConnection();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gio_hang);
        init();
        setuptoolbar();
        checkGioHang();
        tinhtongtien();
        event();
        xulyXoaHangTrongGio();

    }

    private void xulyXoaHangTrongGio() {
        listHangMua.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
                builder.setTitle("Delete???");
                builder.setMessage("Do you want to delete");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(MainActivity.manggiohang.size()<=0){
                            txtthongbao.setVisibility(View.VISIBLE);
                        }else {
                            MainActivity.manggiohang.remove(position);
                            ghAdapter.notifyDataSetChanged();
                            tinhtongtien();
                            if(MainActivity.manggiohang.size()<=0){
                                txtthongbao.setVisibility(View.VISIBLE);
                            }else {
                                txtthongbao.setVisibility(View.INVISIBLE);
                                ghAdapter.notifyDataSetChanged();
                                tinhtongtien();
                            }
                        }
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ghAdapter.notifyDataSetChanged();
                        tinhtongtien();
                    }
                });
                builder.show();
                return true;
            }
        });

    }

    public static void tinhtongtien() {
        int tongtien = 0;
        for(int i = 0;i<MainActivity.manggiohang.size();i++){
            tongtien+= MainActivity.manggiohang.get(i).getGiasp();
        }
        DecimalFormat format = new DecimalFormat("###,###,###");
        txttongtien.setText("Giá chỉ :"+format.format(tongtien)+" VND");
    }

    private void event() {

        btnThanhtoan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(MainActivity.manggiohang.size()>0){
                    startActivity(new Intent(GioHangActivity.this,ThongTinKHActivity.class));
                }else {
                    con.showErr(getApplicationContext(),"Khong co san pham trong gio hang");
                }

            }
        });
        btnMuatiep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),MainActivity.class));
            }
        });

    }

    private void checkGioHang() {
        if(MainActivity.manggiohang.size()<=0){
            ghAdapter.notifyDataSetChanged();
            txtthongbao.setVisibility(View.VISIBLE);
            listHangMua.setVisibility(View.INVISIBLE);
        }
        else {
            ghAdapter.notifyDataSetChanged();
            txtthongbao.setVisibility(View.INVISIBLE);
            listHangMua.setVisibility(View.VISIBLE);
        }
    }



    private void setuptoolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void init() {
        listHangMua = (ListView) findViewById(R.id.listGioHang);
        ghAdapter = new GioHangAdapter(getApplicationContext(),MainActivity.manggiohang);
        btnMuatiep = (Button) findViewById(R.id.muatiep);
        btnThanhtoan = (Button) findViewById(R.id.thanhtoan);
        toolbar = (Toolbar) findViewById(R.id.toolbargiohang);
        listHangMua.setAdapter(ghAdapter);
        txtthongbao = (TextView) findViewById(R.id.txtthongbao);
        txttongtien = (TextView) findViewById(R.id.tongtienGioHang);
    }
}
