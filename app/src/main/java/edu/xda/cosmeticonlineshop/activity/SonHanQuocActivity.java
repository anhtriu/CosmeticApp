package edu.xda.cosmeticonlineshop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import edu.xda.cosmeticonlineshop.R;
import edu.xda.cosmeticonlineshop.adapter.SonHanAdapter;
import edu.xda.cosmeticonlineshop.common.CheckConnection;
import edu.xda.cosmeticonlineshop.common.Server;
import edu.xda.cosmeticonlineshop.model.SanPham;

public class SonHanQuocActivity extends AppCompatActivity {
    ListView listSonHan;
    ArrayList<SanPham> arrSonHan;
    SonHanAdapter sonHanAdapter;
    boolean isLoading = false;
    // de hien thi progressbar
    MyHandler myHandler;
    View footerView;
    boolean outofData = false;
    Toolbar toolbarSonHan;CheckConnection con = new CheckConnection();
    int idsh=0;
    int page =1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_son_han_quoc);
        init();
        setUpToolbar();
        getIdLoaiSP();
        if(con.haveNetworkConnection(getApplicationContext()) ){
            loadDuLieuSonHan(page);
            loadMoreData();
        }else {
            con.showErr(getApplicationContext(),"Vui long kiem tra ket noi internet");
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menugiohang:
                startActivity(new Intent(getApplicationContext(),GioHangActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }
    private void loadMoreData() {
        listSonHan.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(SonHanQuocActivity.this,ChiTietSPActivity.class);
                intent.putExtra("THONGTINSP",arrSonHan.get(position));
                startActivity(intent);
            }
        });
        listSonHan.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                // neu dang dung o vi tri cuoi cung cua listview
                if(firstVisibleItem+visibleItemCount == totalItemCount && totalItemCount != 0
                        && isLoading == false && outofData == false){
                    isLoading = true;
                    MyThread myThread = new MyThread();
                    myThread.start();
                }
            }
        });
    }
    // vi load tu internet ve nen se bi lau nen ta tao luong phu de xu ly lay du lieu va
    // cap nhat len UI ta su dung handler de cung cap cong viec cho cac luong lam viec
    // tao class Handler de quan ly
    public class MyHandler extends Handler{
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case 0:
                    listSonHan.addFooterView(footerView);
                    break;
                case 1:
                    loadDuLieuSonHan(++page); // n se xu ly page = page + 1 truoc khi thuc thi ham loadDuLieuSonHan()
                    isLoading = false;
            }
            super.handleMessage(msg);
        }
    }
    public class MyThread extends Thread{
        @Override
        public void run() {
            myHandler.sendEmptyMessage(0);
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Message message = myHandler.obtainMessage(1);
            myHandler.sendMessage(message);
            super.run();

        }
    }

    private void loadDuLieuSonHan(int page) {
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        String url = Server.pathSONHAN+String.valueOf(page);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                int idsp= 0;
                String tensp="";
                Integer giasp=0;
                String hinhanhsp ="";
                String MOTAsp ="";
                int loaisp=0;
                // check xem co data hay k vi du lieu tra ve it nhat luon la [] nen se co length la 2 vi vay ktra xem
                //length co khac 2 khong thi se hien thi progress bar
                if(response != null && response.length()!=2){
                    try {
                        listSonHan.removeFooterView(footerView);
                        JSONArray jsonArray = new JSONArray(response);
                        for(int i = 0; i<jsonArray.length();i++){
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            idsp = jsonObject.getInt("id");
                            giasp = jsonObject.getInt("giasp");
                            tensp = jsonObject.getString("tensp");
                            hinhanhsp = jsonObject.getString("hinhanhsp");
                            MOTAsp = jsonObject.getString("motosp");
                            loaisp = jsonObject.getInt("idloaisp");
                            arrSonHan.add(new SanPham(idsp,tensp,giasp,hinhanhsp,MOTAsp,loaisp));
                            sonHanAdapter.notifyDataSetChanged();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    // neu da het da ta tuc la length = 2 thi se thay doi gia tri cua bien outofdata va k hien thi progress bar nua
                    outofData = true;
                    listSonHan.removeFooterView(footerView);
                    con.showErr(getApplicationContext(),"Da het du lieu");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            // param de truyen len server : duoc de duoi dang  1 hashmap
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> params = new HashMap<String,String>();
                params.put("loaiSanpham",String.valueOf(idsh));
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }

    private void setUpToolbar() {
        setSupportActionBar(toolbarSonHan);
        // tao nut back de quay ve man hinh truoc
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbarSonHan.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 finish();
            }
        });
    }

    private void init() {
        toolbarSonHan = (Toolbar) findViewById(R.id.toolbarsonhan);
        listSonHan = (ListView) findViewById(R.id.listSonHan);
        arrSonHan = new ArrayList<>();
        sonHanAdapter = new SonHanAdapter(getApplicationContext(),arrSonHan);
        listSonHan.setAdapter(sonHanAdapter);
        sonHanAdapter.notifyDataSetChanged();
        LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
        footerView = inflater.inflate(R.layout.progressbar,null);
        myHandler = new MyHandler();

    }

    public void getIdLoaiSP() {
        idsh = getIntent().getIntExtra("loaiSanpham",-1);
    }
}
