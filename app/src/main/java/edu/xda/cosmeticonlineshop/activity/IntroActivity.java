package edu.xda.cosmeticonlineshop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.github.paolorotolo.appintro.AppIntro;

import edu.xda.cosmeticonlineshop.R;
import edu.xda.cosmeticonlineshop.common.SampleSlide;

public class IntroActivity extends AppIntro {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addSlide(SampleSlide.newInstance(R.layout.first_intro));
        addSlide(SampleSlide.newInstance(R.layout.second_intro));
        addSlide(SampleSlide.newInstance(R.layout.third_intro));
        addSlide(SampleSlide.newInstance(R.layout.fouth_intro));
    }
    @Override
    public void onSkipPressed(Fragment currentFragment) {
        startActivity(new Intent(this,MainActivity.class));
        finish();
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
       startActivity(new Intent(this,MainActivity.class));
        finish();
    }

    @Override
    public void onSlideChanged(@Nullable Fragment oldFragment, @Nullable Fragment newFragment) {
        super.onSlideChanged(oldFragment, newFragment);
        // Do something when the slide changes.
    }
}
