package edu.xda.cosmeticonlineshop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;

import edu.xda.cosmeticonlineshop.R;
import edu.xda.cosmeticonlineshop.common.CheckConnection;
import edu.xda.cosmeticonlineshop.model.GioHang;
import edu.xda.cosmeticonlineshop.model.SanPham;

public class ChiTietSPActivity extends AppCompatActivity {
    Toolbar toolbar;
    TextView txttensp,txtgiasp,txtmotasp;
    Spinner spinner;
    ImageView hinhanhspaa;
    Button btnmuahang;
    int idsp= 0;
    String tensp="";
    Integer giasp=0;
    String hinhanhsp ="";
    String MOTAsp ="";
    int loaisp=0;
    CheckConnection con = new CheckConnection();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chi_tiet_sp);
        init();
        setUpActionBar();
        if(con.haveNetworkConnection(getApplicationContext()) ){
            
            loadDulieuChiTietsp();
        }else {
            con.showErr(getApplicationContext(),"Vui long kiem tra ket noi internet");
        }
        xulySpinner();
        event();
    }

    private void event() {
        btnmuahang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MainActivity.manggiohang.size() > 0){
                    int sl = Integer.parseInt(spinner.getSelectedItem().toString());
                    boolean exist = false;
                    for(int i = 0;i < MainActivity.manggiohang.size();i++){
                        if(MainActivity.manggiohang.get(i).getIdsp()==idsp){
                            MainActivity.manggiohang.get(i).setSoluongsp(MainActivity.manggiohang.get(i).getSoluongsp()+sl);
                            if(MainActivity.manggiohang.get(i).getSoluongsp()>=10){
                                MainActivity.manggiohang.get(i).setSoluongsp(10);
                            }
                            MainActivity.manggiohang.get(i).setGiasp(giasp*MainActivity.manggiohang.get(i).getSoluongsp());
                            exist = true;
                        }
                    }
                    if(exist == false){
                        int soluong = Integer.parseInt(spinner.getSelectedItem().toString());
                        long giamoi = soluong * giasp;
                        MainActivity.manggiohang.add(new GioHang(idsp,tensp,giamoi,hinhanhsp,soluong));
                    }

                }else {
                    int soluong = Integer.parseInt(spinner.getSelectedItem().toString());
                    long giamoi = soluong * giasp;
                    MainActivity.manggiohang.add(new GioHang(idsp,tensp,giamoi,hinhanhsp,soluong));
                }
                Intent i = new Intent(getApplicationContext(),GioHangActivity.class);
                    startActivity(i);
            }
        });

    }

    private void xulySpinner() {
        Integer[] mang = new Integer[]{1,2,3,4,5,6,7,8,9,10};
        ArrayAdapter<Integer> adapter = new ArrayAdapter<Integer>(this,android.R.layout.simple_spinner_dropdown_item,mang);
        spinner.setAdapter(adapter);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menugiohang:
                startActivity(new Intent(getApplicationContext(),GioHangActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }

    private void loadDulieuChiTietsp() {

        SanPham sp = (SanPham) getIntent().getSerializableExtra("THONGTINSP");
        idsp = sp.getId();
        tensp = sp.getTensp();
        giasp = sp.getGiasp();
        hinhanhsp = sp.getHinhanhsp();
        MOTAsp = sp.getMotosp();
        loaisp = sp.getMaloaisp();
        txttensp.setText(tensp);
        DecimalFormat d = new DecimalFormat("###,###,###");
        txtmotasp.setText(MOTAsp);
        txtgiasp.setText("Gia :"+d.format(sp.getGiasp())+" VND");
        Picasso.with(getApplicationContext()).load(sp.getHinhanhsp()).placeholder(R.drawable.ad).error(R.drawable.al).into(hinhanhspaa);


    }

    private void setUpActionBar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });
    }

    private void init() {
        hinhanhspaa = (ImageView) findViewById(R.id.imgChitiet);
        toolbar = (Toolbar) findViewById(R.id.toolbarchitietsp);
        txtgiasp = (TextView) findViewById(R.id.giachitiet);
        txttensp = (TextView) findViewById(R.id.tenChitiet);
        txtmotasp = (TextView) findViewById(R.id.chitietmotasp);
        spinner = (Spinner) findViewById(R.id.spinner);
        btnmuahang = (Button) findViewById(R.id.muahang);

    }
}
