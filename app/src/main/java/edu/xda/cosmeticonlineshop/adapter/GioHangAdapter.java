package edu.xda.cosmeticonlineshop.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.List;

import edu.xda.cosmeticonlineshop.activity.GioHangActivity;
import edu.xda.cosmeticonlineshop.activity.MainActivity;
import edu.xda.cosmeticonlineshop.R;
import edu.xda.cosmeticonlineshop.model.GioHang;

/**
 * Created by Administrator on 11/05/2017.
 */

public class GioHangAdapter extends BaseAdapter {
    Context context;
    List<GioHang> arrGiohang;

    public GioHangAdapter(Context context, List<GioHang> arrGiohang) {
        this.context = context;
        this.arrGiohang = arrGiohang;
    }

    @Override
    public int getCount() {
        return arrGiohang.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v;
        LayoutInflater inflater = LayoutInflater.from(context);
        v = inflater.inflate(R.layout.dong_sp_giohang,null);
        ImageView imgGH;
        final TextView soluongmuaGH,txttenspGH,txtgiaspGH;
        final Button btbgiam,btntang;
        imgGH = (ImageView) v.findViewById(R.id.imgGH);
        soluongmuaGH = (TextView) v.findViewById(R.id.soluongspmua);
        txtgiaspGH = (TextView) v.findViewById(R.id.giaGH);
        txttenspGH = (TextView) v.findViewById(R.id.tenspGioHang);
        btbgiam = (Button) v.findViewById(R.id.giamspgiohang);
        btntang = (Button) v.findViewById(R.id.tangspmua);
        GioHang gh = arrGiohang.get(position);
        txttenspGH.setText(gh.getTensp());
        DecimalFormat format = new DecimalFormat("###,###,###");
        txtgiaspGH.setText("Giá :"+format.format(gh.getGiasp())+" VND");
        Picasso.with(context).load(gh.getHinhsp()).into(imgGH);
        soluongmuaGH.setText(String.valueOf(gh.getSoluongsp()));
        int sl = Integer.parseInt(soluongmuaGH.getText().toString());
        if(sl>=10){
            btntang.setVisibility(View.INVISIBLE);
            btbgiam.setVisibility(View.VISIBLE);
        }else if(sl<=1){
            btntang.setVisibility(View.VISIBLE);
            btbgiam.setVisibility(View.INVISIBLE);
        }else if(sl>1){
            btntang.setVisibility(View.VISIBLE);
            btbgiam.setVisibility(View.VISIBLE);
        }
        btntang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int soluongmoinhat = Integer.parseInt(soluongmuaGH.getText().toString())+1;
                int slht = MainActivity.manggiohang.get(position).getSoluongsp();
                long giaht = MainActivity.manggiohang.get(position).getGiasp();
                MainActivity.manggiohang.get(position).setSoluongsp(soluongmoinhat);
                long giamoinhat = (int) ((giaht*soluongmoinhat)/slht);
                MainActivity.manggiohang.get(position).setGiasp(giamoinhat);
                DecimalFormat format = new DecimalFormat("###,###,###");
                txtgiaspGH.setText("Giá :"+format.format(giamoinhat)+" VND");
                GioHangActivity.tinhtongtien();
                if(soluongmoinhat>9){
                    btntang.setVisibility(View.INVISIBLE);
                    btbgiam.setVisibility(View.VISIBLE);
                    soluongmuaGH.setText(String.valueOf(soluongmoinhat));
                }
                else {
                    btntang.setVisibility(View.VISIBLE);
                    btbgiam.setVisibility(View.VISIBLE);
                    soluongmuaGH.setText(String.valueOf(soluongmoinhat));
                }
            }
        });
        btbgiam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int soluongmoinhat = Integer.parseInt(soluongmuaGH.getText().toString())-1;
                int slht = MainActivity.manggiohang.get(position).getSoluongsp();
                long giaht = MainActivity.manggiohang.get(position).getGiasp();
                MainActivity.manggiohang.get(position).setSoluongsp(soluongmoinhat);
                long giamoinhat = (int) ((giaht*soluongmoinhat)/slht);
                MainActivity.manggiohang.get(position).setGiasp(giamoinhat);
                DecimalFormat format = new DecimalFormat("###,###,###");
                txtgiaspGH.setText("Giá :"+format.format(giamoinhat)+" VND");
                GioHangActivity.tinhtongtien();
                if(soluongmoinhat<2){
                    btntang.setVisibility(View.VISIBLE);
                    btbgiam.setVisibility(View.INVISIBLE);
                    soluongmuaGH.setText(String.valueOf(soluongmoinhat));
                }
                else {
                    btntang.setVisibility(View.VISIBLE);
                    btbgiam.setVisibility(View.VISIBLE);
                    soluongmuaGH.setText(String.valueOf(soluongmoinhat));
                }
            }
        });
        return v;
    }
}
