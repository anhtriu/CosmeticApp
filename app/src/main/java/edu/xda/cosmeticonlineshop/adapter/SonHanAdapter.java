package edu.xda.cosmeticonlineshop.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;

import edu.xda.cosmeticonlineshop.R;
import edu.xda.cosmeticonlineshop.model.SanPham;

/**
 * Created by Administrator on 11/05/2017.
 */

public class SonHanAdapter extends BaseAdapter {
    Context context;
    ArrayList<SanPham> arrSonHan;

    public SonHanAdapter( Context context, ArrayList<SanPham> arrSonHan) {
        this.arrSonHan = arrSonHan;
        this.context = context;
    }

    @Override
    public int getCount() {
        return arrSonHan.size();
    }

    @Override
    public Object getItem(int position) {
        return arrSonHan.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View view, ViewGroup parent) {
       View v;
        ImageView imgSonHan;
        TextView tensonHan,txtGiaSonHan,txtMoTaSonHan;
            LayoutInflater inflater = LayoutInflater.from(context);
            v = inflater.inflate(R.layout.dong_sp_son_han,null);
            tensonHan = (TextView) v.findViewById(R.id.dongTensp);
            txtGiaSonHan = (TextView) v.findViewById(R.id.giaSonHan);
            txtMoTaSonHan = (TextView) v.findViewById(R.id.motaSonHan);
            imgSonHan = (ImageView) v.findViewById(R.id.imgSonHan);

        SanPham sp = arrSonHan.get(position);
        tensonHan.setText(sp.getTensp());
        DecimalFormat d = new DecimalFormat("###,###,###");
        txtGiaSonHan.setText("Giá chỉ :"+d.format(sp.getGiasp())+" VND");
        txtMoTaSonHan.setMaxLines(2);
        txtMoTaSonHan.setEllipsize(TextUtils.TruncateAt.END);
        txtMoTaSonHan.setText(sp.getMotosp());
        Picasso.with(context).load(sp.getHinhanhsp()).placeholder(R.drawable.ad).error(R.drawable.al).into(imgSonHan );
        return v;
    }
}
