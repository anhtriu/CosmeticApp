package edu.xda.cosmeticonlineshop.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import edu.xda.cosmeticonlineshop.R;
import edu.xda.cosmeticonlineshop.model.LoaiSanPha;

/**
 * Created by Administrator on 10/05/2017.
 */

public class LoaiSPAdapter extends BaseAdapter{
    Context context;
    List<LoaiSanPha> loaiSanPhaList;
    public LoaiSPAdapter(Context context, List<LoaiSanPha> loaiSanPhaList){
        this.loaiSanPhaList = loaiSanPhaList;
        this.context = context;

    }
    @Override
    public int getCount() {
        return loaiSanPhaList.size();
    }

    @Override
    public Object getItem(int position) {
        return loaiSanPhaList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
       View v;
        LayoutInflater inflater = LayoutInflater.from(context);
        v = inflater.inflate(R.layout.row_loai_sp,null);
        TextView txtTenloaisp = (TextView) v.findViewById(R.id.rowLoaisp);
        ImageView imageView = (ImageView) v.findViewById(R.id.rowImage);
        LoaiSanPha lsp = loaiSanPhaList.get(position);
        txtTenloaisp.setText(lsp.getTenloaisp());
        Picasso.with(context).load(lsp.getHinhanhLoaisp()).placeholder(R.drawable.ad)
                .error(R.drawable.al)
                .into(imageView);
        return v;
    }
}
