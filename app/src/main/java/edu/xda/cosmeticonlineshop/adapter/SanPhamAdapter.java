package edu.xda.cosmeticonlineshop.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;

import edu.xda.cosmeticonlineshop.activity.ChiTietSPActivity;
import edu.xda.cosmeticonlineshop.R;
import edu.xda.cosmeticonlineshop.model.SanPham;

/**
 * Created by Administrator on 10/05/2017.
 */

public class SanPhamAdapter extends RecyclerView.Adapter<SanPhamAdapter.ItemViewHolder>{
    ArrayList<SanPham> arrayList ;
    Context context;
    public SanPhamAdapter(Context context, ArrayList<SanPham> arrayList){
        this.context = context;
        this.arrayList = arrayList;

    }
    class ItemViewHolder extends RecyclerView.ViewHolder{
        public ImageView hinhsp;
        public  TextView tensp,  giasp;

        public ItemViewHolder(View itemView) {
            super(itemView);
             hinhsp = (ImageView) itemView.findViewById(R.id.imgSpMoiNhat);
             tensp = (TextView) itemView.findViewById(R.id.tenspmoinhat);
             giasp = (TextView) itemView.findViewById(R.id.giaspmoinhat);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, ChiTietSPActivity.class);
                    intent.putExtra("THONGTINSP",arrayList.get(getPosition()));
                    // muon start activity phai dung tren 1 man hinh thi moi goi ham dc nen phai dung context.startactivity
                    // phai dung flag
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });
        }
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.row_sp_moinhat,null);
        ItemViewHolder i = new ItemViewHolder(v);
        return i;
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        SanPham sp = arrayList.get(position);
        holder.tensp.setText(sp.getTensp());
        DecimalFormat d = new DecimalFormat("###,###,###");
        holder.giasp.setText("Gia :"+d.format(sp.getGiasp())+" VND");
        Picasso.with(context).load(sp.getHinhanhsp()).placeholder(R.drawable.ad).error(R.drawable.al).into(holder.hinhsp );
    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }
}
